import nltk
import json
import csv
from timebankpttoolkit import timebankptcorpus


class ErrorPred():
    def __init__(self, sentence, fn_events, fp_events):
        self.sentence = sentence
        self.fn_events = fn_events
        self.fp_events = fp_events
    

def read_error_file(error_filename):
    with open(error_filename) as error_json:
        data = json.load(error_json)
        erros = []
        for p in data:
            sentence = p['sentence']
            words = nltk.word_tokenize(sentence)
            fn_words = [words[pos] for pos, _ in p['fn'] if pos < len(words)]
            fp_words = [words[pos] for pos, _ in p['fp'] if pos < len(words)]
            erros.append(ErrorPred(sentence, fn_words, fp_words))
        return erros

def get_fn_errors_attrib(corpus, error_pred):
    sentence, document = corpus.get_sentence(error_pred.sentence)
    if not sentence:
        return None
    fn_events = []
    events = sentence.get_events()
    pos_last = 0
    for fn_event in error_pred.fn_events:
        for e in events[pos_last:]:
            if e.text == fn_event:
                event = e
                pos_last += 1
                fn_events.append({'document': document.name,
                          'sentence': sentence.text,
                          'sentence_size': len(sentence.text),
                          'sentence_num_events': sentence.num_events(),
                          'event': e.text,
                          'class': e.eclass,
                          'tense': e.tense,
                          'polarity': e.polarity,
                          'pos': e.pos})
                break
    return fn_events
                          
        

def save_history(errors_attrib, filename):
    with open(filename, 'w', newline='') as csvfile:
        histwriter = csv.writer(csvfile, delimiter=',',  quotechar=' ',quoting=csv.QUOTE_MINIMAL)
        error_keys = list(errors_attrib[0].keys())
        histwriter.writerow(error_keys)
        error_len = len(errors_attrib)
        for line in [[str(e[k]) for k in error_keys] for e in errors_attrib]:
            histwriter.writerow(line)



if __name__ == '__main__':
    
    TIMEBANKPT_PATH = '../data/TimeBankPT/'
    ERROR_FILE_PATH = '../data/ti/error_devset.json'
    ERROR_ANALYSE_PATH = '../data/ti/error_analyse.csv'
    
    tbpt = timebankptcorpus.TimeBankPTCorpus('tbpt', TIMEBANKPT_PATH)
    tbpt.load(TIMEBANKPT_PATH)

    errors = []
    for error_pred in read_error_file(ERROR_FILE_PATH):
        errors_fn = get_fn_errors_attrib(tbpt, error_pred)
        if errors_fn:
            errors += errors_fn
    save_history(errors, ERROR_ANALYSE_PATH)
