import regex as re
from timebankpttoolkit.event import Event
from timebankpttoolkit.timex3 import Timex3

class Sentence():

    def parse(self, elm):
        self.events = [Event().parse(e_elm) for e_elm in elm.findall('EVENT')]
        timex3s = elm.findall('TIMEX3')
        self.time_expressions = [Timex3().parse(e_elm) for e_elm in timex3s] if timex3s else []
        self.text = "".join(elm.itertext()).rstrip().replace('\n',' ')
        return self


    def get_positions_events(self, tokenize_word, tokenizer_equal):
        i = 0
        next_event = 0
        num_events = len(self.events)
        self.position_event = {}
        last_event_pos = -1
        sentence_tokens = tokenize_word(self.text)
        for event in self.events:
            i = last_event_pos + 1
            for token in sentence_tokens[i:]:
                if tokenizer_equal(token, tokenize_word(event.text)):
                    self.position_event[i] = event
                    last_event_pos = i
                    break
                i += 1
        return self.position_event

        
    def get_sentence_event_labels(self, sentence_tokens, tokenize_word = (lambda w : w), tokenizer_equal = (lambda x,y: x.lower() == y.lower()), event_label_positive=True):
        events_positions = list(self.get_positions_events(tokenize_word, tokenizer_equal).keys())
        num_word_tokens = len(sentence_tokens)
        
        return [1*event_label_positive if event_label_positive and i in events_positions else (not event_label_positive)*1 for i in range(num_word_tokens)]
            
        
    def has_events(self):
        return len(self.events) > 0

    def num_events(self):
        return len(self.events)

    def get_events(self):
        return self.events

    def get_time_expressions(self):
        return self.time_expressions
    
    def get_text(self):
        return self.text

    def get_events_locations(self):
        return self._get_locations(self.events)

    def get_timexs_locations(self):
        return self._get_locations(self.time_expressions)
        
    def _get_locations(self, text_elms):
        locations = []
        start_at = end_at = 0
        for txt_elm in text_elms:
            elm_str = txt_elm.get_text()
            start_at = self.get_text().find(elm_str, end_at)
            end_at = start_at + len(elm_str)
            locations.append(((start_at, end_at), txt_elm))
        return locations
