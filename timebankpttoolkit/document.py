from  functools import reduce
from timebankpttoolkit.sentence import Sentence


class Document():

    def __init__(self, name=''):
        self.name = name

    
    def parse(self, elm):
        self.sentences_list = [Sentence().parse(s_elm) for s_elm in elm.findall('s')]
        self.sentences_dict = {}
        for sentence in self.sentences_list:
            self.sentences_dict[sentence.text] = sentence
        self.text = "".join(elm.itertext()).strip()

        return self

    def get_sentence(self, text):
        return self.sentences_dict.get(text)
    
    def sentences_without_events(self):
        return [sent for sent in self.sentences_list if not sent.has_events]

    def num_events(self):
        return reduce(lambda a,b: a+b, [s.num_events() for s in self.sentences_list])

    def num_sentences(self):
        return len(self.sentences_list)

    def get_sentences(self):
        return self.sentences_list
    
    def write_out(self, filename, one_sentence_line=True, encoding='utf-8'):
        end_of_sent = ''
        if one_sentence_line == True:
            end_of_sent = '\n'
        num_lines = 0
        with open(filename, 'w', encoding=encoding) as out:
            for sent in self.get_sentences():
                out.write(sent.text + end_of_sent)
                num_lines += 1
        if self.num_sentences() != num_lines:
            print("Num of sentences: {} Num of lines: {}".format(self.num_sentences(), num_lines))
        
