
class Timex3():

    def parse(self, elm):
        attrib = elm.attrib

        self.tid = attrib.get('tid')
        self.text = ''.join(elm.itertext())
        self.ttype = attrib.get('type')
        self.value = attrib.get('value')
        self.mod = attrib.get('mod')
        self.temporal_function = attrib.get('temporalFunction')
        self.function_in_document = attrib.get('functionInDocument')
        self.anchor_time_id = attrib.get('achorTimeID')
        
        return self

    def get_text(self):
        return self.text

    def get_id(self):
        return self.tid
    
