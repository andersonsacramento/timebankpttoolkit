

class Event():

    def parse(self, elm):
        attrib = elm.attrib

        self.eid = attrib['eid']
        self.text = "".join(elm.itertext())
        self.eclass = attrib['class']
        self.stem = attrib['stem']
        self.aspect = attrib['aspect']
        self.tense = attrib['tense']
        self.polarity = attrib['polarity']
        self.pos = attrib['pos']

        return self

    def get_text(self):
        return self.text

    def get_id(self):
        return self.eid

    def get_stem(self):
        return self.stem

    def get_pos(self):
        return self.pos
# equivalent root.findall('s') and list(root.iter('s'))

        
