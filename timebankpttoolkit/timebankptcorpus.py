import os
from functools import reduce
import xml.etree.ElementTree as ET

import timebankpttoolkit.document as document
import timebankpttoolkit.corpus as corpus


class TimeBankPTCorpus(corpus.Corpus):

    def __init__(self, name='', directory='', load=True):
        super().__init__(name, directory)
        if load:
            self.load(directory)


    def load(self, directory, target_dirs=['train','test'], target_ext='.tml'):
        super().load(directory, target_dirs, target_ext)
        
    def parse(self, filename):
        tree = ET.parse(filename)
        root = tree.getroot()
        document_name = filename.split('.tml')[-2].split(os.sep)[-1]
        return document.Document(document_name).parse(root)
        

    def documents_by_target(self, target=None):
        if target:
            documents = self.documents[target]
            if documents:
                return documents
            else:
                return {}
        else:
            return {**self.documents['train'], **self.documents['test']}

    def document_by_pathname(self, pathname):
        splits = pathname.split('.')
        if len(split) > 1:
            filename = splits[-1].split(os.sep)[-1]
        else:
            filename = splits[-2].split(os.sep)[-1]
        return self.documents_by_target()[filename]
        

    def write_documents_text_out(self, new_dir=None, extension='txt', target=None):
        for doc in self.documents_by_target(target).values():
            if new_dir and extension:
                write_path = '{}{}{}.{}'.format(new_dir, os.sep, doc.name.split(os.sep)[-1].split('.tml')[0], extension)
            elif extension:
                write_path = '{}.{}'.format( doc.name.split('.tml')[0], extension)
            else:
                write_path = doc.name
            print(write_path)
            doc.write_out(write_path)
                
    def num_events(self, target=None):
        return reduce(lambda a,b: a+b, [doc.num_events() for doc in self.documents_by_target(target).values()])
        
    def num_sentences(self, target=None):
        return reduce(lambda a,b: a+b, [doc.num_sentences() for doc in self.documents_by_target(target).values()])

    def get_sentence(self, text, target=None):
        for doc in self.documents_by_target(target).values():
            sentence = doc.get_sentence(text)
            if sentence:
                return sentence, doc
        return None, None
