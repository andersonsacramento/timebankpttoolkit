import os
import glob

class Corpus():

    def __init__(self, name='', directory=''):
        self.name = name
        self.directory = directory


    def load(self, directory, target_dirs=[], target_ext=''):
        self.directory = directory
        self.documents = {}
        
        for target_dir in target_dirs:
            target_path = os.path.join(self.directory, target_dir)
            if os.path.isdir(target_path):
                self.documents[target_dir] = {}
                for filename in glob.glob('{}/*{}'.format(target_path, target_ext)):
                    document = self.parse(filename)
                    self.documents[target_dir][document.name] = document
                
    def parse(self, elm):
        pass




    
